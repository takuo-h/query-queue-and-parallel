#! -*- coding:utf-8 -*-

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d/%H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))

# ------------------------------------------------------
import argparse
from collections import namedtuple
def get_config():
	args = argparse.ArgumentParser()
	args.add_argument('--workroom',		type=str)
	args.add_argument('--gpu',			type=int)
	args.add_argument('--message',		type=str)	# message
	args.add_argument('--repeat',		type=int)	# repeat message
	args = vars(args.parse_args())
	return namedtuple('_',args.keys())(**args)

# -------------------------------------------------------------------	
import os
def main():
	config	= get_config()

	results =[]
	for _ in range(config.repeat):
		results.append(f'message:{config.message} with gpu:{config.gpu}')

	with open(os.path.join(config.workroom, 'result.txt'),'w') as fp:
		fp.write('\n'.join(results))

# -------------------------------------
if __name__ == '__main__':
	report('start')
	main()
	report('finish')

