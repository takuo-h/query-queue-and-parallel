#! -*- coding:utf-8 -*-

from os import path
import query_queue_and_parallel as qqp
def main():
	query = qqp.Query()
	query['scripts']	= [path.join(path.dirname(__file__),'experiment01.py')]
	query['message']	= ['hello','world']
	query['repeat']		= [1,2,3]
	query.ready()
	query.run(GPU_index=[0,1])
#	query.run(GPU_index=[-1,-1])

if __name__ == '__main__':
	main()
