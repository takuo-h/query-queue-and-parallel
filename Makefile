.PHONY: deploy
deploy: build
	twine upload dist/*

.PHONY: test-deploy
test-deploy: build
	twine upload -r pypitest dist/*

.PHONY: local
local: build
	python setup.py install

clean:
	pip uninstall query_queue_and_parallel

.PHONY: update
update:
	rm -f -r query_queue_and_parallel.egg-info/* 
	rm -f -r dist/*
	python setup.py sdist bdist_wheel

build:
	python setup.py sdist bdist_wheel
